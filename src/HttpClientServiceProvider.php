<?php

declare(strict_types = 1);
/**
 * 服务提供者，在composer中暴露
 */

namespace Gupo\HttpClient;

use Gupo\HttpClient\Console\InstallCommand;
use Illuminate\Support\ServiceProvider;

class HttpClientServiceProvider extends ServiceProvider
{
    public function register()
    {
    }

    /**
     * 启动所有的应用服务
     *
     * @return void
     */
    public function boot()
    {
        $this->registerCommands(); // 注册-自定义命令行脚本

        $this->registerPublishing(); // 注册-可发布资源

    }

    /**
     * 注册-自定义命令行脚本
     *
     * @author lyl
     */
    public function registerCommands(): void
    {
        if (! $this->app->runningInConsole()) { // 非-控制台中运行
            return;
        }

        $this->commands([
            InstallCommand::class,
        ]);
    }

    /**
     * 注册-可发布资源
     *
     * @author lyl
     */
    public function registerPublishing(): void
    {
        if (! $this->app->runningInConsole()) { // 非-控制台中运行
            return;
        }

        // 发布Api服务
        $this->publishes([
            __DIR__ . '/../stubs/Api/ApiService.stub'             => app_path('Services/Api/ApiService.php'),
            __DIR__ . '/../stubs/Api/Modules/.gitkeep'            => app_path('Services/Api/Modules/.gitkeep'),
            __DIR__ . '/../stubs/Api/Support/BaseHttpClient.stub' => app_path('Services/Api/Support/BaseHttpClient.php'),
            __DIR__ . '/../stubs/config/apis.stub'                => config_path('apis.php'),
        ], 'gupo:http-client:api-service');

    }


}
