<?php

declare(strict_types = 1);
/**
 * 辅助第三方请求的Http请求客户端
 */

namespace Gupo\HttpClient;

use Cloudladder\Http\Client;
use Gupo\HttpClient\Exceptions\HttpClientException;
use Gupo\HttpClient\Traits\HandlerStackTrait;
use Gupo\HttpClient\Traits\LogTrait;
use Gupo\HttpClient\Traits\MiddlewareTrait;
use Gupo\HttpClient\Traits\ResponseTrait;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Str;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Throwable;

abstract class HttpClient
{
    use HandlerStackTrait;
    use MiddlewareTrait;
    use ResponseTrait;
    use LogTrait;

    /**
     * @var string|null 基础uri，通过'getBaseUri'方法的返回值初始化
     * 如果同一厂商的站点地址变更，请在"发起请求前"重写该属性
     */
    protected ?string $base_uri;

    /**
     * @var LoggerInterface|null 日志实例，通过'getLogger'方法的返回值初始化
     */
    public ?LoggerInterface $logger = null;

    /**
     * @var string|null 异常前缀
     * 如果要个性化定义某个类的"异常前缀"，请重写该属性
     */
    protected ?string $default_exception_prefix = null;

    /**
     * @var int|null 超时时间（单位：秒）
     */
    protected ?int $default_timeout = 10;

    /**
     * @var bool 是否执行"为4xx或5xx响应抛出异常"的中间件：true-执行；false-不执行；
     */
    protected bool $default_http_errors = true;

    /**
     * @var string|null 请求Uuid
     */
    public ?string $requestUuid = null;

    /**
     * @var ResponseInterface|null 响应实例
     */
    public ?ResponseInterface $response = null;


    /**
     * @throws HttpClientException
     */
    public function __construct(...$argument)
    {
        // 如果Api类需要进行额外的"个性化的初始化"，则封装到“init”方法中
        if (method_exists($this, 'init')) {
            $this->init(...$argument);
        }

        if (empty($this->base_uri = $this->getBaseUri())) {
            throw new HttpClientException('系统错误，请初始化客户端的 Base Uri！', 0, null, $this);
        }

        $this->logger = $this->getLogger();
    }

    /**
     * 获取-基础Uri
     *
     * @author lyl
     */
    abstract public function getBaseUri(): string;

    /**
     * 获取-日志实例
     *
     * @author lyl
     */
    abstract public function getLogger(): LoggerInterface;

    /**
     * 获取-Api名称
     *
     * @author lyl
     */
    abstract public function getApiName(): string;

    /**
     * 异常前缀
     *
     * @param string|null $exceptionPrefix
     * @param int|null    $err_code
     * @return string
     * @author lyl
     */
    public function exceptionPrefix(?string $exceptionPrefix = null, ?int $err_code = null): string
    {
        return ! is_null($exceptionPrefix)
            ? $exceptionPrefix
            : (! is_null($this->default_exception_prefix)
                ? $this->default_exception_prefix
                : ($this->getApiName() . 'Api异常：')
            );
    }

    /**
     * 发起GET请求
     *
     * @param string        $uri             请求地址
     * @param array         $query           请求数据-query
     * @param array         $middlewareList  中间件列表
     * @param callable|null $handleException 自定义异常处理
     * @param array         $options         请求配置
     * @return ResponseInterface|null
     * @author lyl
     */
    final protected function get(
        string    $uri,
        array     $query = [],
        array     $middlewareList = [],
        ?callable $handleException = null,
        array     $options = []
    )
    {
        return $this->request(
            'GET',
            $uri,
            $query,
            [],
            [],
            $middlewareList,
            $handleException,
            $options
        );
    }

    /**
     * 发起POST请求
     *
     * @param string        $uri             请求地址
     * @param array         $query           请求数据-query
     * @param array         $json            请求数据-json
     * @param array         $formParams      请求数据-form_params
     * @param array         $middlewareList  中间件列表
     * @param callable|null $handleException 自定义异常处理
     * @param array         $options         请求配置
     * @return ResponseInterface|null
     * @author lyl
     */
    final protected function post(
        string    $uri,
        array     $query = [],
        array     $json = [],
        array     $formParams = [],
        array     $middlewareList = [],
        ?callable $handleException = null,
        array     $options = []
    )
    {
        return $this->request(
            'POST',
            $uri,
            $query,
            $json,
            $formParams,
            $middlewareList,
            $handleException,
            $options
        );
    }

    /**
     * 发起请求
     *
     * @param string        $method
     * @param string        $uri
     * @param array         $query
     * @param array         $json
     * @param array         $formParams
     * @param array         $middlewareList 中间件列表
     * @param callable|null $handleException
     * @param array         $options
     * @return ResponseInterface|null
     * @author lyl
     */
    final protected function request(
        string    $method,
        string    $uri,
        array     $query = [],
        array     $json = [],
        array     $formParams = [],
        array     $middlewareList = [],
        ?callable $handleException = null,
        array     $options = []
    ): ?ResponseInterface
    {
        $this->requestUuid = Str::uuid()->toString();

        try {
            if ($query) {
                $options[RequestOptions::QUERY] = $query;
            }

            if ($json) {
                $options[RequestOptions::JSON] = $json;
            }

            if ($formParams) {
                $options[RequestOptions::FORM_PARAMS] = $formParams;
            }

            $this->pushMiddleware($middlewareList);
            $options['handler'] = $this->getHandlerStack();

            $client = $this->getClient(
                $options
            );

            // 发送请求
            /** @var Response $response */
            switch ($method) {
                case 'GET':
                    $response = $client->get(ltrim($uri, '/'), []);
                    break;
                case 'POST':
                    $response = $client->post(ltrim($uri, '/'), []);
                    break;
                default:
                    throw new HttpClientException('不支持的请求方式' . $method, 0, null, $this);
            }

        } catch (\Throwable $exception) {
            $response ??= ($this->response ?? null);

            if (is_callable($handleException)) {
                $handleException($exception, $response);
            } else {
                $this->handleException($exception, $response);
            }
        } finally {
            // 清空-中间件处理实例
            $this->clearHandlerStack();

            // 清空-requestUuid
            $this->requestUuid = null;

            // 清空-响应
            $this->response = null;
        }
        return $response;
    }

    /**
     * 获取http客户端
     *
     * @param array $options
     * @return Client
     * @author lyl
     */
    final public function getClient(
        array $options = []
    ): Client
    {
        $config = [
            'base_uri'                  => $this->base_uri,
            RequestOptions::TIMEOUT     => $this->default_timeout,
            RequestOptions::HTTP_ERRORS => $this->default_http_errors,
        ];
        $config = array_merge($config, $options);

        return new Client($config);
    }

    /**
     * 处理异常
     * 请求过程中出现异常时，默认调用当前类。如果"全局-个性化处理异常"，请在子类中"重写"该方法；如果"单请求-个性化处理异常"，请在调用请求方法时传入"回调方法"
     *
     * @param Throwable              $exception
     * @param ResponseInterface|null $response
     * @throws HttpClientException
     * @throws Throwable
     * @author lyl
     */
    protected function handleException(\Throwable $exception, ?ResponseInterface $response): void
    {
        if ($exception instanceof HttpClientException) {
            throw $exception;
        } else {
            throw new HttpClientException($exception->getMessage(), 0, null, $this, null, $response);
        }
    }


}
