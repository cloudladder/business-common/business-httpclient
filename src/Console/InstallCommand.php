<?php

declare(strict_types=1);
/**
 * HttpClient安装
 */

namespace Gupo\HttpClient\Console;

use Illuminate\Console\Command;
use Exception;

class InstallCommand extends Command
{
    protected $signature = 'gupo:http-client:install';

    protected $description = '安装：第三方请求Http请求客户端';

    /**
     * 执行
     *
     * @throws Exception
     * @author lyl
     */
    public function handle()
    {
        // 发布-资源文件
        $this->publishesResources();

        // 注册-类的别名
        $this->registerAliase();
    }

    /**
     * 发布-资源文件
     *
     * @author lyl
     */
    protected function publishesResources()
    {
        $this->comment('发布"Api服务"基础文件');
        $this->callSilent('vendor:publish', [ '--tag' => 'gupo:http-client:api-service' ]);
    }

    /**
     * 注册-类的别名
     *
     * @throws \Exception
     * @author lyl
     */
    protected function registerAliase()
    {
        $aliase     = 'Api';
        $aliaseName = "\\App\\Services\\Api\\ApiService";

        $this->comment('注册"类的别名：' . $aliase . " -> " . $aliaseName . '"到"config/app.php"');

        // 注册"服务提供者"到"config/app.php"
        registerAliase($aliase, $aliaseName);
    }

}
