<?php

declare(strict_types = 1);
/**
 * 特性-中间件处理实例
 */

namespace Gupo\HttpClient\Traits;

use Gupo\HttpClient\Exceptions\HttpClientException;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;

trait HandlerStackTrait
{
    /** @var HandlerStack|null 中间件处理实例 */
    private ?HandlerStack $handlerStack = null;

    final protected function getHandlerStack()
    {
        if (is_null($this->handlerStack)) {
            $this->handlerStack = new HandlerStack();
            $this->handlerStack->setHandler(new CurlHandler());
        }
        return $this->handlerStack;
    }

    /**
     * 头部的中间件（可重写）
     *
     * @return array<string|int, callable>
     * @author lyl
     */
    protected function headMiddlewareList(): array
    {
        return [
            'add_request_uuid' => $this->addRequestUuid(),
            'add_request_log'  => $this->addRequestLog(),
        ];
    }

    /**
     * 尾部的中间件（建议根据系统实际情况重写）
     *
     * @return array<string|int, callable>
     * @author lyl
     */
    protected function tailMiddlewareList(): array
    {
        return [
            'handle_response'  => $this->handleResponse(),
            'add_response_log' => $this->addResponseLog(),
        ];
    }

    /**
     * push中间件列表
     *
     * @param array<string|int, callable> $middlewareList 中间件列表（请求执行前：先进先出；请求执行后：先进后出）
     * @throws HttpClientException
     * @author lyl
     */
    private function pushMiddleware(array $middlewareList): void
    {
        // 固定-头部的中间件
        $fixHeadMiddlewareList = [
        ];

        // 固定-尾部的中间件
        $fixTailMiddlewareList = [
            'set_response_to_instance' => $this->setResponseToInstance(),
        ];

        // 头部的中间件
        $headMiddlewareList = array_merge(
            $fixHeadMiddlewareList,
            $this->headMiddlewareList()
        );

        // 尾部的中间件
        $tailMiddlewareList = array_merge(
            $this->tailMiddlewareList(),
            $fixTailMiddlewareList
        );

        // 合并中间件
        $mergeMiddlewareList = array_merge($headMiddlewareList, $middlewareList, $tailMiddlewareList);

        if (count($mergeMiddlewareList) != count($headMiddlewareList) + count($middlewareList) + count($tailMiddlewareList)) {
            throw new HttpClientException('系统异常，Http客户端中间件别名冲突！', 0, null, $this);
        }

        // 将中间件列表push到HandlerStack
        $handlerStack = $this->getHandlerStack();
        foreach ($mergeMiddlewareList as $name => $middleware) {
            $middlewareName = is_int($name) ? '' : $name;

            $handlerStack->push($middleware, $middlewareName);
        }
    }

    /**
     * 清空-中间件处理实例
     *
     * @author lyl
     */
    final protected function clearHandlerStack()
    {
        $this->handlerStack = null;
    }

}
