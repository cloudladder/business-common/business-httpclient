<?php
declare(strict_types = 1);

namespace Gupo\HttpClient\Traits;

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\App;

/**
 * 快速构建类的实例
 */
trait InstanceMake
{
    /**
     * Make Class
     *
     * 非单例/支持构建函数传参/不支持构造函数依赖注入
     *
     * @param array $parameters
     *
     * @return static
     */
    public static function make(...$parameters)
    {
        return new static(...$parameters);
    }

    /**
     * instance
     *
     * @param array ...$parameters
     *
     * @return static
     */
    public static function instance(...$parameters)
    {
        App::singletonIf(static::class, function (Application $app) use ($parameters) {
            return new static(...$parameters);
        });

        return App::make(static::class);
    }
}
