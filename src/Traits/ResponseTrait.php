<?php

declare(strict_types = 1);
/**
 * 特性-响应
 */

namespace Gupo\HttpClient\Traits;

use Gupo\HttpClient\Exceptions\HttpClientException;
use Illuminate\Support\Arr;
use Psr\Http\Message\ResponseInterface;

trait ResponseTrait
{
    /**
     * 获取响应内容
     *
     * @param ResponseInterface $response
     * @param int               $type 数据类型：1-字符串 2-对象 3-数组
     * @return mixed
     * @author lyl
     */
    final public function getResponseContents(ResponseInterface $response, int $type = 3)
    {
        $response_body = (string) $response->getBody(); // 响应内容
        switch ($type) {
            case 1:
                return $response_body;
            case 2:
                return json_decode($response_body);
            case 3:
                return json_decode($response_body, true);
            default:
                throw new HttpClientException('系统异常，获取响应内容的数据类型不合法！', 0, null, $this);
        }
    }

    /**
     * 获取响应中的值，默认返回"data"键中的值
     *
     * @param ResponseInterface $response
     * @param string            $key
     * @return mixed
     * @author lyl
     */
    final public function getResponseValue(ResponseInterface $response, string $key = 'data')
    {
        $response_json = $this->getResponseContents($response);
        return Arr::get($response_json, $key);
    }
}
