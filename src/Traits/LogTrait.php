<?php

declare(strict_types=1);
/**
 * 特性-日志
 */

namespace Gupo\HttpClient\Traits;

use Psr\Log\LoggerInterface;

trait LogTrait
{
    /**
     * 添加日志
     *
     * @param string $method
     * @param        ...$content
     * @author lyl
     */
    public function addLog(string $method, ...$content)
    {
        $logger = $this->logger;
        if ($logger instanceof LoggerInterface) {
            $logger->$method(...$content);
        }
    }

}
