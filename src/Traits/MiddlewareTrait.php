<?php

declare(strict_types=1);
/**
 * 特性-给出的基础中间件
 */

namespace Gupo\HttpClient\Traits;

use Gupo\HttpClient\Exceptions\HttpClientException;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

trait MiddlewareTrait
{
    /**
     * 添加Uuid到请求Headers
     *
     * @return callable
     * @author lyl
     */
    protected function addRequestUuid()
    {
        return Middleware::mapRequest(function (RequestInterface $request) {
            return $request->withHeader('request-uuid', $this->requestUuid);
        });
    }

    /**
     * 添加请求日志
     *
     * @return callable
     * @author lyl
     */
    protected function addRequestLog(): callable
    {
        return Middleware::mapRequest(function (RequestInterface $request) {
            $this->addLog('info', $this->getApiName() . ' Request', [
                'uuid'    => $this->requestUuid,
                'uri'     => (string) $request->getUri(),
                'body'    => json_decode((string) $request->getBody(), true),
                'method'  => $request->getMethod(),
                'headers' => $request->getHeaders(),
            ]);
            return $request;
        });
    }

    /**
     * 添加响应日志
     *
     * @return callable
     * @author lyl
     */
    protected function addResponseLog(): callable
    {
        return Middleware::mapResponse(function (ResponseInterface $response) {
            $this->addLog('info', $this->getApiName() . ' Response', [
                'uuid'     => $this->requestUuid,
                'status'   => $response->getStatusCode(),
                'contents' => $this->getResponseContents($response),
            ]);
            return $response;
        });
    }

    /**
     * 处理响应
     *
     * @return callable
     * @author lyl
     */
    protected function handleResponse(): callable
    {
        return Middleware::mapResponse(function (ResponseInterface $response) {
            $response_status   = $response->getStatusCode(); // 状态码
            $response_contents = $this->getResponseContents($response); // 响应内容-json

            $err_msg = ($response_contents['message'] ?? null)
                ?: ($response_contents['error_msg'] ?? null)
                    ?: ($response_contents['errormsg'] ?? null);

            if (200 != $response_status) {
                $err_msg = $err_msg ?: '接口服务异常';
                throw new HttpClientException($err_msg, 0, null, $this);
            }

            if (($response_contents['code'] ?? null) != 200) {
                $err_msg = $err_msg ?: '未知异常';
                throw new HttpClientException($err_msg, 0, null, $this);
            }

            return $response;
        });
    }

    /**
     * 将【响应结果】写入【实例】
     *
     * @return callable
     * @author lyl
     */
    final protected function setResponseToInstance(): callable
    {
        return Middleware::mapResponse(function (ResponseInterface $response) {
            $this->response = $response;

            return $response;
        });
    }

}
