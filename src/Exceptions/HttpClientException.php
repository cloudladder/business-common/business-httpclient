<?php

declare(strict_types=1);
/**
 * 客户端异常
 */

namespace Gupo\HttpClient\Exceptions;

use Gupo\HttpClient\HttpClient;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class HttpClientException extends \Exception implements \Throwable
{
    public function __construct(
        $message = "",
        $code = 0,
        ?Throwable $previous = null,
        ?HttpClient $httpClientInstance = null,
        ?RequestInterface $request = null,
        ResponseInterface $response = null
    ) {
        if ($httpClientInstance) {
            $message = $httpClientInstance->exceptionPrefix(null, $code) . $message;
        }

        $this->addExceptionLog($message, $code, $httpClientInstance, $request, $response);
        parent::__construct($message, $code, $previous);
    }

    public function addExceptionLog(
        $message = "",
        $code = 0,
        ?HttpClient $httpClientInstance = null,
        ?RequestInterface $request = null,
        ResponseInterface $response = null
    ) {
        if (! is_null($httpClientInstance)) {
            $logContent = [
                'uuid'      => $httpClientInstance->requestUuid,
                'exception' => [
                    'err_msg'  => $message,
                    'err_file' => $this->getFile(),
                    'err_line' => $this->getLine(),
                    'err_code' => $this->getCode(),
                ]
            ];


            $response = $response ?? $httpClientInstance->response;
            if ($response instanceof ResponseInterface) {
                $logContent['response'] = [
                    'status_code' => $response->getStatusCode(),
                    'content'     => $httpClientInstance->getResponseContents($response, 3),
                ];
            }

            $httpClientInstance->addLog('error', $httpClientInstance->getApiName() . ' Request Exception', $logContent);
        }
    }
}
